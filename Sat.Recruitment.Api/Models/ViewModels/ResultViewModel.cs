﻿namespace Sat.Recruitment.Api.Models.ViewModels
{
    public class ResultViewModel
    {
        public bool IsSuccess { get; set; }
        public string Errors { get; set; }
    }
}
