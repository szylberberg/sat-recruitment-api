﻿using Microsoft.AspNetCore.Mvc;
using Sat.Recruitment.Api.Models.ViewModels;
using Sat.Recruitment.Api.Services;

namespace Sat.Recruitment.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public partial class UsersController : ControllerBase
    {
        private readonly UserService _userService;

        public UsersController(UserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        [Route("create-user")]
        public ResultViewModel CreateUser(UserViewModel userVM)
        {
            var result = _userService.CreateUser(userVM);
            return result;
        }
    }
}
