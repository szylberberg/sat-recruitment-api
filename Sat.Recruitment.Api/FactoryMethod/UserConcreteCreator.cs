﻿using System;

namespace Sat.Recruitment.Api
{
    public class UserConcreteCreator
    {
        private const string NORMAL_USER = "Normal";
        private const string SUPER_USER = "SuperUser";
        private const string PREMIUM_USER = "Premium";

        public static UserCreator GetNewUser(string userType)
        {
            switch (userType)
            {
                case NORMAL_USER:
                    return new NormalUser();
                case SUPER_USER:
                    return new SuperUser();
                case PREMIUM_USER:
                    return new PremiumUser();
                default:
                    throw new Exception("User type does not exists");
            }
        }
    }
}
