﻿using Sat.Recruitment.Api.Models;
using Sat.Recruitment.Api.Models.ViewModels;
using System;

namespace Sat.Recruitment.Api
{
    public class NormalUser : UserCreator
    {
        public override User CreateUser(UserViewModel newUserVM)
        {
            User userEntity = GetNewEntityUser(newUserVM);

            if (userEntity.Money <= 100)
            {
                if (userEntity.Money > 10)
                {
                    var percentage = Convert.ToDecimal(0.8);
                    var gif = userEntity.Money * percentage;
                    userEntity.Money += gif;
                }
            }
            else
            {
                var percentage = Convert.ToDecimal(0.12);
                var gif = userEntity.Money * percentage;
                userEntity.Money += gif;
            }

            return userEntity;
        }
    }
}
