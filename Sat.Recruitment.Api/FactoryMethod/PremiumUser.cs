﻿using Sat.Recruitment.Api.Models;
using Sat.Recruitment.Api.Models.ViewModels;

namespace Sat.Recruitment.Api
{
    public class PremiumUser : UserCreator
    {
        public override User CreateUser(UserViewModel newUserVM)
        {
            User userEntity = GetNewEntityUser(newUserVM);

            if (userEntity.Money > 100)
            {
                var gif = userEntity.Money * 2;
                userEntity.Money += gif;
            }

            return userEntity;
        }
    }
}
