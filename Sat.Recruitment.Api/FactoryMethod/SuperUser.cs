﻿using Sat.Recruitment.Api.Models;
using Sat.Recruitment.Api.Models.ViewModels;
using System;

namespace Sat.Recruitment.Api
{
    public class SuperUser : UserCreator
    {
        public override User CreateUser(UserViewModel newUserVM)
        {
            User userEntity = GetNewEntityUser(newUserVM);

            if (userEntity.Money > 100)
            {
                var percentage = Convert.ToDecimal(0.20);
                var gif = userEntity.Money * percentage;
                userEntity.Money += gif;
            }

            return userEntity;
        }
    }
}
