﻿using Sat.Recruitment.Api.Helpers;
using Sat.Recruitment.Api.Models;
using Sat.Recruitment.Api.Models.ViewModels;

namespace Sat.Recruitment.Api
{
    public abstract class UserCreator
    {
        public User GetNewEntityUser(UserViewModel newUserVM)
        {
            decimal.TryParse(newUserVM.Money, out decimal parsedMoney);

            return new User
            {
                Name = newUserVM.Name,
                Email = Helper.NormalizeEmail(newUserVM.Email),
                Address = newUserVM.Address,
                Phone = newUserVM.Phone,
                UserType = newUserVM.UserType,
                Money = parsedMoney
            };
        }
        public abstract User CreateUser(UserViewModel newUserVM);
    }
}
