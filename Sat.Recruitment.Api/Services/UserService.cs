﻿using Sat.Recruitment.Api.Models;
using Sat.Recruitment.Api.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Sat.Recruitment.Api.Services
{
    public class UserService
    {
        /// <summary>
        /// Creates a new user
        /// </summary>
        public ResultViewModel CreateUser(UserViewModel userVM)
        {
            var errors = ValidateUser(userVM);

            if (errors != string.Empty)
                return new ResultViewModel()
                {
                    IsSuccess = false,
                    Errors = errors
                };

            try
            {
                var newEntity = UserConcreteCreator.GetNewUser(userVM.UserType);
                var newUser = newEntity.CreateUser(userVM);

                var duplicatedUser = GetDuplicatedUser(newUser);

                if (duplicatedUser != null)
                {
                    //user already exists
                    Debug.WriteLine("The user is duplicated");
                    return new ResultViewModel()
                    {
                        IsSuccess = false,
                        Errors = "The user is duplicated"
                    };
                }
                else
                {
                    Debug.WriteLine("User Created");
                    return new ResultViewModel()
                    {
                        IsSuccess = true,
                        Errors = "User Created"
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResultViewModel()
                {
                    IsSuccess = false,
                    Errors = ex.Message
                };
            }
        }

        /// <summary>
        /// Search user in existing user list
        /// </summary>
        private User GetDuplicatedUser(User newUser)
        {
            List<User> existingUsersList = GetExistingUsers();
            var user = existingUsersList.Find(u => (u.Email == newUser.Email ||
                                                     u.Phone == newUser.Phone) ||
                                                   (u.Name == newUser.Name &&
                                                     u.Address == newUser.Address));

            return user;
        }

        /// <summary>
        /// Returns existing user list
        /// </summary>
        /// <returns></returns>
        private List<User> GetExistingUsers()
        {
            try
            {
                List<User> existingUsers = new List<User>();
                var reader = ReadUsersFromFile();

                while (reader.Peek() >= 0)
                {
                    var line = reader.ReadLineAsync().Result;
                    var existingUser = new User
                    {
                        Name = line.Split(',')[0].ToString(),
                        Email = line.Split(',')[1].ToString(),
                        Phone = line.Split(',')[2].ToString(),
                        Address = line.Split(',')[3].ToString(),
                        UserType = line.Split(',')[4].ToString(),
                        Money = decimal.Parse(line.Split(',')[5].ToString()),
                    };
                    existingUsers.Add(existingUser);
                }
                reader.Close();

                return existingUsers;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error on GetExistingUsers: {ex}");
            }
        }

        /// <summary>
        /// Validate if user data has errors
        /// </summary>
        private string ValidateUser(UserViewModel userToValidate)
        {
            string errors = string.Empty;

            if (string.IsNullOrEmpty(userToValidate.Name))
                errors = "The name is required";
            if (string.IsNullOrEmpty(userToValidate.Email))
                errors += " The email is required";
            if (string.IsNullOrEmpty(userToValidate.Address))
                errors += " The address is required";
            if (string.IsNullOrEmpty(userToValidate.Phone))
                errors += " The phone is required";

            return errors;
        }

        /// <summary>
        /// Returns the existing list of users
        /// </summary>
        private StreamReader ReadUsersFromFile()
        {
            var path = Directory.GetCurrentDirectory() + "/Files/Users.txt";

            FileStream fileStream = new FileStream(path, FileMode.Open);

            StreamReader reader = new StreamReader(fileStream);
            return reader;
        }
    }
}
