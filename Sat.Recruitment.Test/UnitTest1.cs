using Sat.Recruitment.Api.Controllers;
using Sat.Recruitment.Api.Models.ViewModels;
using Sat.Recruitment.Api.Services;
using Xunit;

namespace Sat.Recruitment.Test
{
    [CollectionDefinition("Tests", DisableParallelization = true)]
    public class UnitTest1
    {
        [Fact]
        public void TestOkUserCreated()
        {
            var userController = new UsersController(userService: new UserService());

            UserViewModel userVM = new UserViewModel
            {
                Name = "Mike",
                Email = "mike@gmail.com",
                Address = "Av. Juan G",
                Phone = "+349 1122354215",
                UserType = "Normal",
                Money = "124"
            };

            var result = userController.CreateUser(userVM);

            Assert.True(result.IsSuccess);
            Assert.Equal("User Created", result.Errors);
        }

        [Fact]
        public void TestDuplicatedUser()
        {
            var userController = new UsersController(userService: new UserService());

            UserViewModel userVM = new UserViewModel
            {
                Name = "Agustina",
                Email = "Agustina@gmail.com",
                Address = "Av. Juan G",
                Phone = "+349 1122354215",
                UserType = "Normal",
                Money = "124"
            };

            var result = userController.CreateUser(userVM);

            Assert.False(result.IsSuccess);
            Assert.Equal("The user is duplicated", result.Errors);
        }

        [Fact]
        public void TestUserDuplicatedByNameAndAddress()
        {
            var userController = new UsersController(userService: new UserService());

            UserViewModel userVM = new UserViewModel
            {
                Name = "Franco",
                Email = "frg@gmail.com",
                Address = "Alvear y Colombres",
                Phone = "+5491165233251",
                UserType = "Premium",
                Money = "125"
            };

            var result = userController.CreateUser(userVM);

            Assert.False(result.IsSuccess);
            Assert.Equal("The user is duplicated", result.Errors);
        }

        [Fact]
        public void TestUserDuplicatedByAddressNotName()
        {
            var userController = new UsersController(userService: new UserService());

            UserViewModel userVM = new UserViewModel
            {
                Name = "Carlos",
                Email = "cgolib@gmail.com",
                Address = "Peru 2464",
                Phone = "+549116523888",
                UserType = "Normal",
                Money = "125"
            };

            var result = userController.CreateUser(userVM);

            Assert.True(result.IsSuccess);
            Assert.Equal("User Created", result.Errors);
        }

        [Fact]
        public void TestRequiredField()
        {
            var userController = new UsersController(userService: new UserService());

            UserViewModel userVM = new UserViewModel
            {
                Name = "",
                Email = "tito@gmail.com",
                Address = "Tres Arroyos 27",
                Phone = "45556562",
                UserType = "Normal",
                Money = "125"
            };

            var result = userController.CreateUser(userVM);

            Assert.False(result.IsSuccess);
            Assert.Equal("The name is required", result.Errors);
        }

        [Fact]
        public void TestInvalidEmail()
        {
            var userController = new UsersController(userService: new UserService());

            UserViewModel userVM = new UserViewModel
            {
                Name = "Franco",
                Email = "xxxxxxxx",
                Address = "Test 123",
                Phone = "+5491165233251",
                UserType = "Premium",
                Money = "125"
            };

            var result = userController.CreateUser(userVM);

            Assert.False(result.IsSuccess);
            Assert.Equal("Error at Normalize Email", result.Errors);
        }

        [Fact]
        public void TestInvalidUserType()
        {
            var userController = new UsersController(userService: new UserService());

            UserViewModel userVM = new UserViewModel
            {
                Name = "Franco",
                Email = "frl@gmail.com",
                Address = "Test 123",
                Phone = "+5491165233251",
                UserType = null,
                Money = "125"
            };

            var result = userController.CreateUser(userVM);

            Assert.False(result.IsSuccess);
            Assert.Equal("User type does not exists", result.Errors);
        }
    }
}
